import 'package:bloc/bloc.dart';

class NumberCubit extends Cubit<int> {
  NumberCubit() : super(0);

  void increase() => emit(this.state+1);
  void decrease() => emit(this.state-1);
}
