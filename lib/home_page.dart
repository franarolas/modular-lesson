import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:untitled/cubit/number_cubit.dart';
import 'package:untitled/second_page.dart';

class HomePage extends StatelessWidget {
  static const String ROUTE = '/';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            Text('HOME'),
            BlocBuilder<NumberCubit, int>(bloc: Modular.get<NumberCubit>(), builder: (context, state) {
              return Text('$state');
            }),
            ElevatedButton(
                onPressed: () => Modular.to.pushNamed(SecondPage.ROUTE), child: Text('Navigate to second page')),
            ElevatedButton(
                onPressed: () => Modular.get<NumberCubit>().increase(), child: Text('Increase'))
          ],
        ),
      ),
    );
  }
}
