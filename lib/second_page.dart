import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:untitled/cubit/number_cubit.dart';

class SecondPage extends StatelessWidget {
  static const String ROUTE = '/second';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Column(
          children: [
            Text('SECOND'),
            ElevatedButton(onPressed: () => Modular.get<NumberCubit>().decrease(), child: Text('Decrease'))
          ],
        ),
      ),
    );
  }
}
