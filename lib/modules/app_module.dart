import 'package:flutter_modular/flutter_modular.dart';
import 'package:untitled/cubit/number_cubit.dart';
import 'package:untitled/home_page.dart';
import 'package:untitled/modules/home_module.dart';
import 'package:untitled/second_page.dart';

class AppModule extends Module {
  @override
  final List<Bind> binds = [
    Bind.lazySingleton((_) => NumberCubit()),
  ];

  @override
  final List<ModularRoute> routes = [
    ModuleRoute(HomePage.ROUTE, module: HomeModule()),
    ChildRoute(SecondPage.ROUTE, child: (_, args) => SecondPage()),
  ];

}