

import 'package:flutter_modular/flutter_modular.dart';
import 'package:untitled/home_page.dart';

class HomeModule extends Module {
  @override
  final List<ModularRoute> routes = [
    ChildRoute(HomePage.ROUTE, child: (_, __) => HomePage())
  ];

}