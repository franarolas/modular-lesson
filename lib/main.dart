import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:untitled/home_page.dart';
import 'package:untitled/modules/app_module.dart';

void main() {
  runApp(ModularApp(module: AppModule(), child: App()));
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Modular demo',
      initialRoute: HomePage.ROUTE,
    ).modular();
  }
}